import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:theme_changer/views/splash/splash_controller.dart';

import '../../style/dimensions.dart';
import '../../utils/image_paths.dart';

class SplashView extends GetView<SplashController> {
  SplashView({super.key});

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return GetBuilder(
      init: SplashController(),
      builder: (SplashController controller) {
        return Scaffold(
          body: Container(
            color: Theme.of(context).colorScheme.primary,
            height: Dimensions.screenHeight,
            width: Dimensions.screenWidth,
            child: Stack(
              children: [
                Center(
                  child: Image.asset(
                    ImagePath.splashBg,
                    fit: BoxFit.fitWidth,
                    height: Dimensions.screenHeight / 2,
                    width: Dimensions.screenWidth,
                  ),
                ),
                Center(
                  child: Image.asset(
                    ImagePath.splashIcon,
                    color: Theme.of(context).colorScheme.onSecondary,
                    width: Dimensions.screenWidth / 1.07,
                    height: Dimensions.screenHeight / 5.5,
                  ),
                ).paddingOnly(bottom: 30),
              ],
            ),
          ),
        );
      },
    );
  }
}
