import 'dart:async';

import 'package:get/get.dart';

import '../../controller/base_controller.dart';
import '../../routes/app_pages.dart';
import '../../style/dimensions.dart';

class SplashController extends BaseController {

  @override
  void onInit() {
    splashTimer();
    super.onInit();
  }

  void splashTimer() async {
    var _duration = Duration(
      seconds: Dimensions.screenLoadTime,
    );
    Future.delayed(_duration, () async {
      Get.offNamedUntil(Routes.LOGIN, (route) => false);
    });
  }

}
