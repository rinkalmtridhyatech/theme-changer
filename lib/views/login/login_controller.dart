import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../controller/base_controller.dart';
import '../../style/appThemes.dart';
import '../../style/getStorageKey.dart';

class LoginController extends BaseController {
  late AutovalidateMode autovalidateMode;

  late TextEditingController emailController;
  late TextEditingController passwordController;
  late FocusNode emailFocusNode;
  late FocusNode passwordFocusNode;
  var obscureText = true.obs;
  late final GetStorage _getStorage;
  var isDarkMode = false.obs;

  @override
  void onInit() {
    super.onInit();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    emailFocusNode = FocusNode();
    passwordFocusNode = FocusNode();
    autovalidateMode = AutovalidateMode.disabled;
    _getStorage = GetStorage();
    isDarkMode.value = _getStorage.read(GetStorageKey.IS_DARK_MODE);
  }

  void changeTheme(BuildContext context) {
    final theme = Get.isDarkMode ? ThemeMode.light : ThemeMode.dark;
    final themeData =
        Get.isDarkMode ? AppThemes.lightThemeData : AppThemes.darkThemeData;
    Get.changeThemeMode(theme);
    Get.changeTheme(themeData);
    if (_getStorage.read(GetStorageKey.IS_DARK_MODE)) {
      _getStorage.write(GetStorageKey.IS_DARK_MODE, false);
      isDarkMode.value = false;
    } else {
      _getStorage.write(GetStorageKey.IS_DARK_MODE, true);
      isDarkMode.value = true;
    }
  }
}
