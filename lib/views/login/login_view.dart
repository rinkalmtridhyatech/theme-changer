import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../style/color_constants.dart';
import '../../../style/dimensions.dart';
import '../../../style/text_styles.dart';
import '../../../utils/app_constants.dart';
import '../../../utils/image_paths.dart';
import '../../../utils/ui_utils.dart';
import '../../style/appColors.dart';
import 'login_controller.dart';

class LoginView extends GetView<LoginController> {
  LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: LoginController(),
        builder: (LoginController controller) {
          return SafeArea(
            bottom: false,
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.background,
                title: Text(
                  "Theme Changer",
                  style: AppTextStyles().textBold.copyWith(fontSize: 22),
                ),
                actions: [
                  Obx(() => IconButton(
                        icon: controller.isDarkMode.value
                            ? const Icon(
                                CupertinoIcons.brightness,
                                color: Colors.white,
                              )
                            : Icon(
                                CupertinoIcons.moon_stars,
                                color: AppColors().kBlackColor,
                              ),
                        onPressed: () {
                          controller.changeTheme(context);
                        },
                      ))
                ],
              ),
              body: NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (OverscrollIndicatorNotification overScroll) {
                    overScroll.disallowIndicator();
                    return false;
                  },
                  child: Container(
                    color: Theme.of(context).colorScheme.primary,
                    height: Dimensions.screenHeight,
                    width: Dimensions.screenWidth,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: Dimensions.screenWidth / 8),
                        Text(
                          AppConstants.signIn,
                          style: AppTextStyles().textBold,
                        ),
                        Text(
                          AppConstants.email,
                          style: AppTextStyles().smallText,
                        ).paddingOnly(left: 6, top: 22),
                        UiUtils()
                            .textField(
                              hintText: AppConstants.enterEmail,
                              autofillHints: [AutofillHints.email],
                              textController: controller.emailController,
                              focusNode: controller.emailFocusNode,
                              inputAction: TextInputAction.next,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return AppConstants.emailEmptyErrorMessage;
                                } else if (!GetUtils.isEmail(value)) {
                                  return AppConstants.emailInvalidErrorMessage;
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.emailAddress,
                              obscureText: false,
                            )
                            .paddingOnly(bottom: 12),
                        Text(
                          AppConstants.password,
                          style: AppTextStyles().smallText,
                        ).paddingOnly(
                          left: 6,
                        ),
                        Obx(
                          () => UiUtils().textField(
                            hintText: AppConstants.enterPassword,
                            suffixPressed: () {
                              controller.obscureText.value =
                                  !controller.obscureText.value;
                            },
                            autofillHints: [AutofillHints.password],
                            textController: controller.passwordController,
                            focusNode: controller.passwordFocusNode,
                            inputAction: TextInputAction.done,
                            requiredObscureText: true,
                            validator: (value) {
                              if (value.isEmpty) {
                                return AppConstants.passwordEmptyErrorMessage;
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.text,
                            obscureText: controller.obscureText.value,
                          ),
                        ),
                        Text(
                          AppConstants.forgotPassword,
                          style: AppTextStyles()
                              .smallText
                              .copyWith(color: ColorConstants.green5ED5A8),
                        ).paddingOnly(left: 6, top: 6, bottom: 40),
                        UiUtils()
                            .appButton(
                                onPressFunction: () {},
                                buttonText: AppConstants.signIn,
                                buttonFillColor: ColorConstants.green5ED5A8,
                                buttonTextColor:
                                    AppColors().kTextFieldTextColor,
                                buttonBorderColor: Colors.transparent)
                            .paddingOnly(bottom: 16),
                      ],
                    ).paddingSymmetric(horizontal: 26),
                  )),
              bottomSheet: Container(
                color: Theme.of(context).colorScheme.primary,
                width: Dimensions.screenWidth,
                height: Dimensions.screenWidth / 4,
                child: Column(
                  children: [
                    SvgPicture.asset(ImagePath.fingerPrint),
                    Text(
                      AppConstants.fingerprint,
                      style: AppTextStyles().smallText,
                    ).paddingOnly(bottom: 6, top: 10),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
