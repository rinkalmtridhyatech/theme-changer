import 'package:flutter/material.dart';

import 'appColors.dart';
import 'dimensions.dart';

class AppTextStyles {
  static const String fontName = 'NeueMontreal';
  var textBold = TextStyle(
    color: AppColors().kPrimaryTextColor,
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: Dimensions.fontSize26,
  );

  var regularText = TextStyle(
    color: AppColors().kPrimaryTextColor,
    fontFamily: fontName,
    fontSize: Dimensions.fontSizeDefault,
  );

  var largeText = TextStyle(
    color: AppColors().kPrimaryTextColor,
    fontFamily: fontName,
    fontSize: Dimensions.fontSizeLarge,
  );

  var smallText = TextStyle(
    color: AppColors().kSmallTextColor,
    fontFamily: fontName,
    fontSize: Dimensions.fontSizeDefault,
  );
}
