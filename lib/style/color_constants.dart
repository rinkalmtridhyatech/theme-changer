import 'package:flutter/material.dart';

class ColorConstants {
  static const white = Colors.white;
  static const black = Colors.black;
  static const black1B232A = Color(0xFF1B232A);
  static const black212931 = Color(0xFF212931);
  static const black161C22 = Color(0xFF161C22);
  static const green5ED5A8 = Color(0xFF5ED5A8);
  static const green1 = Color(0xFF36835A);
  static const redDD4B4B = Color(0xFFDD4B4B);
  static const grayF1F4F6 = Color(0xFFF1F4F6);
  static const grayF1F4F691 = Color(0x91F1F4F6);

  static MaterialColor appColor = const MaterialColor(
    0xFF12B2D5,
    <int, Color>{
      50: Color(0xFF5ED5A8),
      100: Color(0xFF5ED5A8),
      200: Color(0xFF5ED5A8),
      300: Color(0xFF5ED5A8),
      400: Color(0xFF5ED5A8),
      500: Color(0xFF5ED5A8),
      600: Color(0xFF5ED5A8),
      700: Color(0xFF5ED5A8),
      800: Color(0xFF5ED5A8),
      900: Color(0xFF5ED5A8),
    },
  );
}
