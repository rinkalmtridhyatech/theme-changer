part of 'app_pages.dart';

abstract class Routes {
  Routes._();

  static const SPLASH = '/';
  static const LOGIN = '/auth';
}
