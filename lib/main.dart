import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:theme_changer/routes/app_pages.dart';
import 'package:theme_changer/style/appThemes.dart';
import 'package:theme_changer/style/getStorageKey.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  final getStorage = GetStorage();
  bool isDarkMode = getStorage.read(GetStorageKey.IS_DARK_MODE) ?? false;
  getStorage.write(GetStorageKey.IS_DARK_MODE, isDarkMode);

  runApp(Builder(
    builder: (context) => GetMaterialApp(
      title: "Theme Changer",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: isDarkMode ? AppThemes.darkThemeData : AppThemes.lightThemeData,
      darkTheme: AppThemes.darkThemeData,
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.leftToRightWithFade,
    ),
  ));
}
