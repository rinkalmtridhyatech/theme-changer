class AppConstants {
  //App related
  static const String appName = 'Setup App';
  static const int appVersion = 1;

  //Languages
  static const String languageEnglish = "en";
  static const String languageGerman = "de";

  //API related
  static const String baseApi = 'http://restapi.adequateshop.com/api';
  static const String login = '$baseApi/authaccount/auth';

  //API results
  static const String checkInFail = 'Check-In Failed';

  /*
Shared Pref
  */
  static const String apiKey = 'apiKey';

/*
UI Related
  */

//Login
  static const String signIn = 'Sign in';
  static const String signUp = 'Sign up';
  static const String email = 'Email';
  static const String password = 'Password';
  static const String mobileNo = 'Mobile Number';
  static const String forgotPassword = 'Forgot Password?';
  static const String orLoginWith = 'Or login with';
  static const String enterEmail = 'Enter your email';
  static const String enterNumber = 'Enter your number';
  static const String enterPassword = 'Enter your password';
  static const emailEmptyErrorMessage = "Please Enter Email";
  static const numberEmptyErrorMessage = "Please Enter Mobile Number";
  static const passwordEmptyErrorMessage = "Please Enter Password";
  static const emailInvalidErrorMessage = "Invalid Email";
  static const mobileNoInvalidErrorMessage = "Invalid Mobile Number";
  static const sendOtp = "Send OTP";
  static const continueButton = "Continue";
  static const getStarted = "Get Started";
  static const facebook = "Facebook";
  static const google = "Google";
  static const verification = "Verification";
  static const fingerprint = "Use fingerprint instead?";
  static const registerWithMobile = "Register with mobile";
  static const enterYourCode = "Enter your code";
  static const resendCode = "Resend your code";
  static const mobileDetails =
      "Please type your number, then we’ll send a verification code for authentication.";
  static const otpDetails = "Please type the code we sent to";
  static const successOtp = "Your account has been successfully created!";


  //Home
  static const trading = "P2P Trading";
  static const tradingDetails = "Bank Transfer, Paypal Revolut...";
  static const creditCard = "Credit/Debit Card";
  static const visaMasterCard = "Visa,Mastercard";
}
