class ImagePath {
  //Splash
  static const String splashBg = 'assets/images/splashBg.png';
  static const String splashIcon = 'assets/images/appIcon.png';


  //SignIn and SignUp
  static const String fbIcon = 'assets/icons/ic_fb.png';
  static const String googleIcon = 'assets/icons/ic_google.svg';
  static const String fingerPrint = 'assets/icons/ic_fingerPrint.svg';
  static const String successfulRegistration =
      'assets/images/successfulRegistration.png';

}
