import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:theme_changer/style/appColors.dart';

import '../style/color_constants.dart';
import '../style/dimensions.dart';
import '../style/text_styles.dart';

class UiUtils {
  Widget gapWidget({double? height = 0, double? width = 0}) {
    return SizedBox(
      height: height,
      width: width,
    );
  }

  Widget appButton(
      {required VoidCallback? onPressFunction,
      String? buttonText,
      bool isIconButton = false,
      Widget? iconWidget,
      Color? buttonFillColor,
      Color? buttonBorderColor,
      Color? buttonTextColor}) {
    return Container(
      height: 56,
      child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(buttonFillColor),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide(color: buttonBorderColor!)))),
          onPressed: () {
            onPressFunction != null ? onPressFunction() : null;
          },
          child: isIconButton
              ? iconWidget
              : Center(
                  child: Text(
                  buttonText ?? "N/A",
                  style: TextStyle(
                    fontSize: 18,
                    color: buttonTextColor,
                    fontFamily: 'NeueMontreal',
                  ),
                ))),
    );
  }

  EdgeInsets setDiffPadding(
      {double left = 0, double top = 0, double right = 0, double bottom = 0}) {
    return EdgeInsets.fromLTRB(left, top, right, bottom);
  }

  EdgeInsets setAllPadding(double value) {
    return EdgeInsets.all(value);
  }

  Widget appTextButton({
    required VoidCallback onPressFunction,
    String? buttonText,
    Color? buttonTextColor,
  }) {
    return TextButton(
        onPressed: () {
          onPressFunction();
        },
        child: Text(buttonText ?? "N/A",
            style: TextStyle(
              color: buttonTextColor,
              fontSize: Dimensions.screenWidth / 22,
              letterSpacing: 0.25,
              fontWeight: FontWeight.w500,
            )));
  }

  showLogMessage({
    required var className,
    required String functionName,
    required String message,
  }) {
    if (message.isNotEmpty && kDebugMode) {
      return debugPrint(
          "className : $className , functionName: $functionName , message :$message");
    }
  }

  Widget dividerWidget() {
    return const Divider(
      color: ColorConstants.black,
      thickness: 1.0,
    );
  }

  Widget textField({
    String? hintText,
    Function? validator,
    required bool obscureText,
    TextEditingController? textController,
    TextInputType? keyboardType,
    FocusNode? nextFocusNode,
    required FocusNode focusNode,
    required TextInputAction inputAction,
    Function? onSaved,
    bool? requiredObscureText,
    Iterable<String>? autofillHints,
    VoidCallback? suffixPressed,
  }) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
      decoration: BoxDecoration(
        color: AppColors().kTextFieldColor,
        borderRadius: BorderRadius.circular(14),
        border: Border.all(
          color: AppColors().kTextFieldColor,
        ),
      ),
      child: TextFormField(
        autofillHints: autofillHints,
        obscureText: obscureText,
        keyboardType: keyboardType,
        controller: textController,
        textInputAction: inputAction,
        maxLines: 1,
        minLines: 1,
        cursorColor: AppColors().kSmallTextColor,
        validator: (value) {
          return validator!(value);
        },
        focusNode: focusNode.hasFocus ? focusNode : null,
        onSaved: (value) {
          (inputAction == TextInputAction.next)
              ? FocusScope.of(Get.context!).requestFocus(nextFocusNode)
              : null;
          onSaved!(value);
        },
        style: AppTextStyles().regularText,
        decoration: InputDecoration(
            suffixIcon: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (requiredObscureText == true)
                  InkWell(
                      onTap: suffixPressed,
                      highlightColor: Colors.transparent,
                      child: Padding(
                        padding: UiUtils().setDiffPadding(
                            left: 10, right: 10, bottom: 10, top: 14),
                        child: Icon(
                          obscureText
                              ? Icons.visibility_rounded
                              : Icons.visibility_off_rounded,
                          color: AppColors().kSmallTextColor,
                          size: 22,
                        ),
                      )),
              ],
            ),
            hintStyle: TextStyle(
                color: AppColors().kSmallTextColor, fontFamily: "NeueMontreal"),
            filled: true,
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 5.0),
              borderRadius: BorderRadius.circular(25.0),
            ),
            hintText: hintText),
      ),
    );
  }
}
